const jwt = require('jsonwebtoken')


const secret = "CourseBookingAPI"

module.exports.createAccessToken = (user) => {
	const data = {
		id : user.id,
		email : user.email,
		isAdmin : user.isAdmin
	}



	return jwt.sign(data, secret, {})
}

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization

	if(typeof token !== "undefined") {
		console.log("TOKEN")
		console.log(token)
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (error, data) => {
			if(error) {
				return res.send({auth : "failed"})
			} else {
				next()
			}
		})
	} else {
		return res.send({auth : "failed"})
	}
}


module.exports.decode = (token) => {
	if(typeof token !== "undefined") {
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (error, data) => {
			if(error) {
				return null
			} else {
				console.log("DATA ")
				console.log(data)
				// jwt.decode(token, {complete : true}).payload
				return data
			}


		})
	} else {
		return null;
	}
}

module.exports.verifyAdmin = (req,res, next) => {
	let token = req.headers.authorization

	const userData = module.exports.decode(token)
	console.log("USER DATA ADMIN VERIFY")
	console.log(userData)

	if(userData.isAdmin) {
		return next()
	} else {
		return res.send({auth : "Not admin"})
	}
}

module.exports.verifyNotAdmin = (req, res, next) => {
	let token = req.headers.authorization

	const userData = module.exports.decode(token)

	if(userData.isAdmin) {
		return res.send({auth : "is Admin"})
	} else {
		return next()
	}
}


