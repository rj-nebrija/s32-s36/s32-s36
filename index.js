const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const cors = require('cors')
const userRoutes = require('./routes/userRoutes')
const courseRoutes = require('./routes/courseRoutes')

//TEST
const userModel = require('./models/User')


dotenv.config()

mongoose.connect(process.env.MONGO_URI, {
	useNewUrlParser : true,
	useUnifiedTopology : true
})

const db = mongoose.connection
db.on('error', console.error.bind(console, "Connection error"))
db.once('open', ()=> console.log("Connected to database"))


const port = 4000

const app = express()


app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended : true}))
app.use('/users', userRoutes)
app.use('/courses', courseRoutes)

app.listen(process.env.PORT || port , () => console.log(`Listening at port ${process.env.PORT || port}`))