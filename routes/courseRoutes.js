const express = require('express')
const router = express.Router()
const courseController = require('../controllers/courseControllers')
const auth = require('../auth')

router.post('/', auth.verify, auth.verifyAdmin,  (req, res) => {
	courseController.addCourse(req.body).then(result => res.send(result))
})


router.get('/all', auth.verify, (req,res)=> {
	courseController.getAllCourses().then(result => res.send(result))
})

router.get('/', (req,res)=> {
	courseController.getAllActive().then(result=> {
		if(result == false) {
			res.status(400).send("ERROR NO ACTIVE COURSE")
		} else {
			res.send(result)
		}
	})
})


router.get('/:id', (req,res)=> {
	console.log(req.params.id)

	courseController.getCourse(req.params.id).then(result => {
		if(result == false) {
			res.status(400).send("COURSE NOT FOUND")
		} else {
			res.send(result)
		}
	})
})

router.put('/:id', auth.verify, auth.verifyAdmin, (req,res)=> {
	console.log("REQ HEAEDERS")
	console.log(req.headers.authorization)
	const data = {
		courseId : req.params.id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		updatedCourse : req.body
	}

	courseController.updateCourse(data).then(result => {
		res.send(result)
	})
})

router.put('/:id/archive', auth.verify, auth.verifyAdmin, (req,res)=> {
	courseController.archiveCourse(req.params.id).then(result => {
		if (result == false) {
			res.status(400).send("ERROR UPDATING")
		} else {
			res.send("ARCHIVED COURSE")
		}
	})
})

module.exports = router