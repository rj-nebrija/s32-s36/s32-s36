const User = require('../models/User')
const Course = require('../models/Course')
const bcrypt = require('bcrypt')
const auth = require('../auth')

module.exports.checkEmailExists = (body) => {
	return User.find({email : body.email}).then((result) => {
		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
}


module.exports.registerUser = (body) => {
	let newUser = new User({
		firstName : body.firstName,
		lastName : body.lastName,
		email : body.email,
		mobileNo : body.mobileNo,
		password : bcrypt.hashSync(body.password, 10)
	})

	return newUser.save().then((user,err) => {
		if (err) {
			return false
		} else {
			return true
		}
	})
}

module.exports.loginUser = (body) => {
	return User.findOne({email : body.email}).then(result => {
		if(result == null) {
			return false
		} {
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)

			if(isPasswordCorrect) {
				return { access : auth.createAccessToken(result)}
			}
		}
	})
}

module.exports.getProfile = ({userId}) => {
	return User.findById(userId).then(result => {
		if(result == null) {
			return false
		} else {
			console.log(result)
			let newobj = {...result}
			console.log(newobj)
			result.password = ""
			return result
		}	
	})
}


module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId : data.courseId})

		return user.save().then((user,error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	})


	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId : data.userId})

		return course.save().then((user,error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	})

	if(isUserUpdated && isCourseUpdated) {
		return true
	} else {
		return false
	}
}