const Course = require('../models/Course')
const auth = require('../auth')
const mongoose = require('mongoose')


module.exports.addCourse = (body) => {
	let newCourse = new Course({
		name : body.name,
		description : body.description,
		price : body.price,
		isActive : body.isActive
	})

	return newCourse.save().then((course, err) => {
		if(err) {
			return false
		} else {
			return true
		}
	})
} 


module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result
	})
}

module.exports.getAllActive = () => {
	return Course.find({isActive : true}).then(result => {
		if(result) {
			return result
		} else {
			return false
		}
	})
}


module.exports.getCourse = (id) => {
	const id2 = mongoose.Types.ObjectId(id.trim());
	return Course.findById(id2).then((result) => {
		if(result == null) {
			return false
		} else {
			return result
		}
	})
}


module.exports.updateCourse = (data)=> {
	return Course.findById(data.courseId).then((result,error) => {
		console.log(result)

		result.name = data.updatedCourse.name
		result.description = data.updatedCourse.description
		result.price = data.updatedCourse.price

		console.log(result)

		return result.save().then((updatedCourse, error)=> {
			if(error) {
				return false
			} else {
				return updatedCourse
			}
		})
	})
}

module.exports.archiveCourse = (id) => {
	return Course.findByIdAndUpdate(id, {isActive : false}).then((result, err) => {
		if(err) {
			return false
		} else {
			return true
		}
	})
}


//async await example for error handling
module.exports.getCourse2 = async (id) => {
	try {
		const course = await Course.findById(id)
		return course
	} catch(err) {
		console.log("ERROR")
		console.log(err)
		return false
	}
}

